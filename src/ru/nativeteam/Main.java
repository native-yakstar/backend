package ru.nativeteam;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.omg.IOP.Encoding;

import java.io.*;
import java.net.*;
import java.nio.charset.Charset;
import java.sql.*;
import java.util.ArrayList;

public class Main {
    private static final String url = "jdbc:mysql://localhost:3306/AMTEL_MONITORING";
    private static final String user = "root";
    private static final String password = "Qa2WSX741!";

    private static Connection con;
    private static Statement stmt;
    private static ResultSet rs;

    static String USER_AGENT = "Mozilla/5.0";

    static String LOGIN = "barnaul";
    static String PASSWORD = "barnaul";
    static String SKD_NAME = "Анабар";


    public static void main(String[] args) {

        while (true){
            Handle();
        }

    }

    static private void Handle(){
        try {
            System.out.println("Get token");
            String phpSessid = getPHPSessid();

            if (phpSessid.equals(null)){
                System.out.println("Error when get token");
            }else{
                System.out.println("Autorization");
                verifyPHPSESSID(phpSessid, LOGIN, PASSWORD);

                System.out.println("Get information for SKD");
                String SatellitesInfo = getSatellitesInfo(phpSessid, SKD_NAME);

                Document html = Jsoup.parse(SatellitesInfo);
                Elements elements = html.body().getElementsByClass("mytable").get(10).getElementsByTag("tr");


                JSONArray titles = new JSONArray();
                JSONArray speed = new JSONArray();
                JSONArray upload = new JSONArray();
                JSONArray download = new JSONArray();
                JSONArray avail = new JSONArray();

                for (int i = 3; i < elements.size(); i++) {

                    Elements td_elements = elements.get(i).getElementsByTag("td");

                    for (int j = 0; j < td_elements.size(); j++) {

                        switch (j){
                            case 8:{
                                titles.put(td_elements.get(j).text());
                                if (td_elements.text().contains("вкл")){
                                    avail.put("1");
                                }else{
                                    avail.put("0");
                                }

                                break;
                            }
                            case 9:{
                                speed.put(td_elements.get(j).text());


                                break;
                            }
                            case 10:{
                                String _upload = null;
                                String _download = null;
                                if (td_elements.get(j).text() == ""){
                                    i++;
                                }else{
                                    try {
                                        _upload = td_elements.get(j).text().split(" ")[0];
                                        _download = td_elements.get(j).text().split(" ")[1];

                                    }catch (ArrayIndexOutOfBoundsException e){
                                        //e.printStackTrace();
                                    }

                                }

                                upload.put(_upload);
                                download.put(_download);

                                break;
                            }

                        }
                    }
                }


                System.out.println("Update database");

                saveData(titles.toString() , speed.toString() , upload.toString() , download.toString() , avail.toString());

                System.out.println("-----------------------\n");


            }

            Thread.sleep(250000);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    static private String getPHPSessid() {
        try {
            String url = "http://monitoring.amtelcom.ru/index.php";
            URL obj = new URL(url);

            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            //add reuqest header
            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.flush();
            wr.close();


            String PHPSESSID = con.getHeaderFields().toString();
            PHPSESSID = PHPSESSID.split("PHPSESSID=")[1].split(";")[0];

            return PHPSESSID;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    static private String verifyPHPSESSID(String phpSessid, String login, String password) {
        try {
            String url = "http://monitoring.amtelcom.ru/index.php";
            URL obj = new URL(url);

            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            //add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            con.setRequestProperty("Cookie", "PHPSESSID="+phpSessid);

            String urlParameters = "login="+login+"&password="+password;//+"&search_address="+SKDName;

            // Send post request
            con.setDoOutput(true);
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeBytes(urlParameters);
            wr.flush();
            wr.close();


            String PHPSESSID = con.getHeaderFields().toString();
            PHPSESSID = PHPSESSID.split("PHPSESSID=")[1].split(";")[0];

            return PHPSESSID;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    static private String getSatellitesInfo(String PHPSESSID, String skdName){
        try {
            String url = "http://monitoring.amtelcom.ru/index.php";
            URL obj = new URL(url);

            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            //add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Encoding", "identity");
            con.setRequestProperty("Cookie", "PHPSESSID="+PHPSESSID);
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded, charset=UTF-8");
            con.setRequestProperty("Cookie", "PHPSESSID="+PHPSESSID);

            String urlParameters = "action=&" +
                    "fields[TT]=checked&" +
                    "fields[address]=checked&" +
                    "fields[client]=checked&" +
                    "fields[numbering]=checked&" +
                    "fields[provider]=checked&" +
                    "fields[sattelite]=checked&" +
                    "fields[speed]=checked&" +
                    "fields[station]=checked&" +
                    "fields[status]=checked&" +
                    "fields[time]=checked&" +
                    "filter=&" +
                    "hub=all&" +
                    "mon_st=0&" +
                    "providers[]=4&" +
                    "providers[]=7&" +
                    "providers[]=9&" +
                    "providers[]=10&" +
                    "providers[]=12&" +
                    "providers[]=13&" +
                    "sattelites[]=1&" +
                    "sattelites[]=2&" +
                    "sattelites[]=4&" +
                    "sattelites[]=10&" +
                    "sattelites[]=11&" +
                    "sattelites[]=21&" +
                    "sattelites[]=13&" +
                    "sattelites[]=20&" +
                    "sattelites[]=16&" +
                    "sattelites[]=17&" +
                    "sattelites[]=18&" +
                    "scroll_x=&" +
                    "scroll_y=&" +
                    "search_address="+skdName+"&" +
                    "search_ip=&" +
                    "search_ip_mask=&" +
                    "search_siteid=&" +
                    "search_station=&" +
                    "sort_by=&";

            // Send post request
            con.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            wr.writeUTF(urlParameters);
            wr.flush();
            wr.close();
            Charset charset = Charset.forName("UTF-8");

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream(), charset));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();


            return response.toString();

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }

    private static void saveData(String title, String speed, String upload, String download, String avail){
        try {
            String url = "http://monitoring.yakstar.net/upload.php";
            URL obj = new URL(url);

            HttpURLConnection con = (HttpURLConnection) obj.openConnection();

            //add reuqest header
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", USER_AGENT);
            con.setRequestProperty("Accept-Charset", "UTF-8");
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded, charset=UTF-8");

            String urlParameters =
                    "&masterKey=logovotelintel&" +
                    "skd_name=ANABAR&" +
                    "title="+title+"&" +
                    "speed="+speed+"&" +
                    "upload="+upload+"&" +
                    "download="+download+"&"+
                    "avail="+avail;

            // Send post request
            con.setDoOutput(true);

            DataOutputStream wr = new DataOutputStream(con.getOutputStream());


            wr.writeUTF(urlParameters);

            wr.flush();
            wr.close();

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();


            if (response.toString().equals("success")){
                System.out.println("Date succeful saving in database");
            }else{
                System.out.println("Error: " + response.toString());
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
